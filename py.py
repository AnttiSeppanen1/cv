import mysql.connector

yhteys = mysql.connector.connect(
    host="localhost",
    user="Antti",
    password="k1250521",
    database="mydatabase",  
    port=3000
)

kursori = yhteys.cursor()
kursori.execute("CREATE DATABASE IF NOT EXISTS mydatabase")
kursori.execute("USE mydatabase")
kursori.execute("""
CREATE TABLE IF NOT EXISTS lapsitaulu (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nimi VARCHAR(255) NOT NULL,
    ika INT NOT NULL,
    laskutus INT NOT NULL
)
""")

kolmoset = []
pikkuset = []
isot = []

def lisaa(nimi, ika, laskutus):
    sql = "INSERT INTO lapsitaulu (nimi, ika, laskutus) VALUES (%s, %s, %s)"
    val = (nimi, ika, laskutus)
    kursori.execute(sql, val)
    yhteys.commit()
    print(kursori.rowcount, "tietue lisätty.")
    lapsi = (nimi, ika, laskutus)
    if ika == 3:
        kolmoset.append(lapsi)
        tulosta_kolmoset()
    elif ika > 3:
        isot.append(lapsi)
        tulosta_isot()
    elif ika < 3:
        pikkuset.append(lapsi)
        tulosta_pikkuset()

def tulosta_tiedot():
    kursori.execute("SELECT * FROM lapsitaulu")
    tulokset = kursori.fetchall()
    for tulos in tulokset:
        print(tulos)
        
def tulosta_kolmoset():
    kursori.execute("SELECT * FROM lapsitaulu WHERE ika = 3")
    tulokset = kursori.fetchall()
    for tulos in tulokset:
        print(tulos)
        
def tulosta_pikkuset():
    kursori.execute("SELECT * FROM lapsitaulu WHERE ika < 3")
    tulokset = kursori.fetchall()
    for tulos in tulokset:
        print(tulos)
        
def tulosta_isot():
    kursori.execute("SELECT * FROM lapsitaulu WHERE ika > 3")
    tulokset = kursori.fetchall()
    for tulos in tulokset:
        print(tulos)
        
def ryhmä_valinta():
    valitse = int(input("Valitse ryhmä: ( 1 = kaikki, 2 = pikkuset, 3 = kolmoset, 4 = isot , 5 = takaisin)"))
    if valitse == 1:
        tulosta_tiedot()
    elif valitse == 2:
        tulosta_pikkuset()
    elif valitse == 3:
        tulosta_kolmoset()
    elif valitse == 4:
        tulosta_isot()
    elif valitse == 5:
        alku()
    else:
        print("Virheellinen valinta, yritä uudelleen.")
def lopeta():
     print("Ohjelma lopetetaan.")
     alku()
     
def suodata():
    peruste = int(input("Millä perusteella suodatetaan: (1 = ikä, 2 = laskutus)"))
    if peruste == 1:
        ika_peruste = int(input("Valitse ikä millä etsitään:"))
        kursori.execute("SELECT * FROM lapsitaulu WHERE ika = %s",(ika_peruste,))
        tulokset = kursori.fetchall()
        for tulos in tulokset:
            print(tulos)
    elif peruste == 2:
        lasku_peruste = int(input("Valitse laskutus numero millä etsitään:"))
        kursori.execute("SELECT * FROM lapsitaulu WHERE laskutus = %s",(lasku_peruste,))
        tulokset = kursori.fetchall()
        for tulos in tulokset:
            print(tulos)
        
def lisää_tiedot():
    print("Lisätään lapsia listaan. Tyhjä lisäys lopettaa")
    while True:
        laskutus = int(input("Valitse lasskutus luokka: (1 = täysi laskutus, 2 = puolet, 3 = kolmasosa)"))
        nimi = input("Anna nimi: ")
        if nimi == "":
            break
        ika = int(input("Anna ikä: "))
        lisaa(nimi, ika, laskutus)
        jatketaanko = input("jatketaanko? (y/n): ")
        if jatketaanko.lower() == "n":
            alku()
            
def muokkaa():
    print("Muokataan lapsia listaan. Tyhjä nimi lopettaa")
    id = int(input("Anna lapsen ID numero:"))
    uusi_nimi = input("Anna nimi: ")
    uusi_ika = int(input("Anna ikä: "))
    kursori.execute("UPDATE lapsitaulu SET nimi = %s, ika = %s WHERE id = %s", (uusi_nimi, uusi_ika, id))

def alku():
    global kolmoset, pikkuset, isot

    while True:
        tehtava = int(input("Mitä tehdään? (1 = lisätään lapsi, 2 = tarkista ryhmät, 3 = lopetetaan ohjelma, 4 = Muokkaa tietoja, 5 = suodata ja näytä): "))
        if tehtava == 1:
            lisää_tiedot()
        elif tehtava == 2:
            ryhmä_valinta()
        elif tehtava == 3:
            lopeta()
        elif tehtava == 4:
            muokkaa()
        elif tehtava == 5:
            suodata()
        else:
            print("Virheellinen valinta, yritä uudelleen.")

alku()

kursori.close()
yhteys.close()
